import sys
import requests

from PyQt5 import QtWidgets

from mainwindow import Ui_MainWindow

class mywindow(QtWidgets.QMainWindow):

    def __init__(self):
        super(mywindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.pushButton.clicked.connect(self.btnClicked)

    def btnClicked(self):
        test1 = self.ui.lineEdit.text()
        test2 = self.ui.lineEdit_2.text()
        req = self.RequestGet('{0}/?{1}'.format(test1, test2), True)
        self.ui.label_3.setText('Url: {0}\nParameter: {1}\nRequest:{3}'.format(test1, test2, req))

    def RequestGet(self, Url, tostring=False):
        r = self.requests.get(Url)
        if (tostring == True):
            content = r.content
            print(content)
        return content


app = QtWidgets.QApplication([])
application = mywindow()
application.show()
sys.exit(app.exec())



#Url = 'http://127.0.0.1:5000/api/v1/resultado/?alumno=LeonardoBack'
#RequestGet(Url, True)