# Objeto
class Rectangulo(object):

    # Constructor
    def __init__(self, alto, ancho):
        self.alto = alto
        self.ancho = ancho

    # Funcion
    def area(self):
        return self.alto * self.ancho

    # Funcion
    def perimetro(self):
        return 2 * self.alto + 2 * self.ancho

# Como se usa: Objeto
rectangulo = Rectangulo(20, 30)
area = rectangulo.area()


# Herencia
class Cuadrado(Rectangulo):

    # Constructor
    def __init__(self, alto):
        super().__init__(alto, alto)  

    # Funcion

# Como se usa: Herencia
cuadrado = Cuadrado(10)
area = cuadrado.area()
