from funciones import *

def user_control():
    repeat = True
    while (repeat):
        command = -1
        while (command < 0 or command > 6):
            print('{0}\tConectar BD'.format(1))
            print('{0}\tGuardar en tabla'.format(2))
            print('{0}\tGuardar registros'.format(3))
            print('{0}\tActualizar registro'.format(4))
            print('{0}\tBorrar registro'.format(5))
            print('{0}\tTraer'.format(6))
            print('{0}\tJoin'.format(7))
            print('{0}\tOrderBy'.format(8))
            print('{0}\tWhere compleja'.format(9))
            print('{0}\tSalir'.format(0))
            command = int(input('=> ingrese numero de la opcion: '))
            print('Opcion Seleccionada: {0}'.format(command))
            

        if (command == 0):
            repeat = False
            print('Aplicacion finalizada')
        elif (command == 1):
            print('Base da datos por defecto: {0}'.format(db.database))
            conectar(input('Base de datos a trabajar [Enter por defecto]: '))
        elif (command == 2):
            crear_tabla_person()
            crear_tabla_pet()
        elif (command == 3):
            guardar_datos_tabla()
            crear_registros()
        elif (command == 4):
            actualizar_registros()
        elif (command == 5):
            borrar_registro()
        elif (command == 6):
            traer()
        elif (command == 7):
            traer_un_registro()
        elif (command == 8):    
            join()
        elif (command == 9): 
            order_by()
        else:
            where_compleja()

    if (repeat == False):
        exit()