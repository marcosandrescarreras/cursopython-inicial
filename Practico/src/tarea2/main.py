# Tarea #2
# Content b'{
#     "tarea": 
#     "Tarea: Descargue los archivos de 
#     https://gist.github.com/ElDwarf/e3d93745e85530e3b8f01e0b9ed3e03a .
#     Luego instale PyQt5. 
#     Complete la logica faltante para poder cargar la interfaz grafica y conectar el evento
#     del boton, pegue la url que se detalla a continuacion el el campo de texto y evalue su trabajo.
#     URL: https://4etjz3ty75.execute-api.us-east-1.amazonaws.com/default/curso_step_3?id=7707766712006331404"
# }'

import sys
import json
import requests

from PyQt5 import QtWidgets

from mainwindow import Ui_MainWindow

def get_petitions(url):
    r = requests.get(url)
    content_json = json.loads(r.content)
    return content_json['result']

class mywindow(QtWidgets.QMainWindow):

    def __init__(self):
        super(mywindow, self).__init__()
        # Completar carga de archivo
        # Conectar señar del boton con el evento btnClicked
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.pushButton.clicked.connect(self.btnClicked)

    def btnClicked(self):
        url = self.ui.lineEdit.text()
        resultado = get_petitions(url)
        self.ui.label_3.setText(resultado)


app = QtWidgets.QApplication([])
application = mywindow()
application.show()
sys.exit(app.exec())